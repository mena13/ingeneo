package sv.com.ingeneo.repository;

import org.springframework.data.repository.CrudRepository;
import sv.com.ingeneo.model.TipoProducto;


public interface TipoProductoRepo extends CrudRepository<TipoProducto,Long> {

    TipoProducto findByCodigo(String codigo);

}
