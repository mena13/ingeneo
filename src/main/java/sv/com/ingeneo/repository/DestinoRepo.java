package sv.com.ingeneo.repository;

import org.springframework.data.repository.CrudRepository;
import sv.com.ingeneo.model.Destino;


public interface DestinoRepo extends CrudRepository<Destino,Long> {

    Destino findByCodigo(String codigo);

}
