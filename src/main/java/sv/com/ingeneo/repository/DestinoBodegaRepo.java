package sv.com.ingeneo.repository;

import org.springframework.data.repository.CrudRepository;
import sv.com.ingeneo.model.DestinoBodega;


public interface DestinoBodegaRepo extends CrudRepository<DestinoBodega,Long> {

    DestinoBodega findByCodigo(String codigo);

}
