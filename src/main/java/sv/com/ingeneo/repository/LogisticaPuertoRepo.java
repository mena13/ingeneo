package sv.com.ingeneo.repository;

import org.springframework.data.repository.CrudRepository;
import sv.com.ingeneo.model.LogisticaBodega;
import sv.com.ingeneo.model.LogisticaPuerto;

public interface LogisticaPuertoRepo extends CrudRepository<LogisticaPuerto,Long> {

}
