package sv.com.ingeneo.repository;

import org.springframework.data.repository.CrudRepository;
import sv.com.ingeneo.model.DestinoPuerto;


public interface DestinoPuertoRepo extends CrudRepository<DestinoPuerto,Long> {

    DestinoPuerto findByCodigo(String codigo);

}
