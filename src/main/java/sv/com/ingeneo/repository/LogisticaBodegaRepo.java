package sv.com.ingeneo.repository;

import org.springframework.data.repository.CrudRepository;
import sv.com.ingeneo.model.Logistica;
import sv.com.ingeneo.model.LogisticaBodega;

public interface LogisticaBodegaRepo extends CrudRepository<LogisticaBodega,Long> {

}
