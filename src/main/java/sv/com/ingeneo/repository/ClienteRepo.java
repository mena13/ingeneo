package sv.com.ingeneo.repository;

import org.springframework.data.repository.CrudRepository;
import sv.com.ingeneo.model.Cliente;

public interface ClienteRepo extends CrudRepository<Cliente,Long> {

    Cliente findByCodigo(String codigo);


}
