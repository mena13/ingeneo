package sv.com.ingeneo.repository;

import org.springframework.data.repository.CrudRepository;
import sv.com.ingeneo.model.Logistica;

public interface LogisticaRepo extends CrudRepository<Logistica,Long> {

}
