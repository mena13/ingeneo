package sv.com.ingeneo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import sv.com.ingeneo.dto.TipoProductoDTO;
import sv.com.ingeneo.model.TipoProducto;
import sv.com.ingeneo.service.TipoProductoService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/tipoproducto")
public class TipoProductoController {

    @Autowired
    TipoProductoService service;

    @GetMapping(name = "/")
    public List<TipoProducto> getAll(){
        return service.getAll();
    }

    @GetMapping(value = "/{id}")
    public TipoProducto getById(@PathVariable("id") Long id) {
        return service.getById(id);
    }

    @PostMapping(value = "/")
    public TipoProducto save(@Valid @RequestBody TipoProductoDTO dto) {
        return service.save(service.convertToEntity(dto));
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable Long id) {
        service.delete(id);
    }

    @GetMapping(value = "/findbycodigo/{codigo}")
    public TipoProducto getById(@PathVariable("codigo") String codigo) {
        return service.getByCodigo(codigo);
    }

}
