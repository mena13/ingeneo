package sv.com.ingeneo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import sv.com.ingeneo.dto.LogisticaDTO;
import sv.com.ingeneo.model.Logistica;
import sv.com.ingeneo.model.LogisticaBodega;
import sv.com.ingeneo.service.LogisticaService;
import sv.com.ingeneo.service.LogisticaServiceImpl;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/logistica")
public class LogisticaController {
    final static Logger logger = LoggerFactory.getLogger(LogisticaController.class);

    @Autowired
    LogisticaService service;


    @GetMapping(name = "/")
    public List<Logistica> getAll(){
        return service.getAll();
    }

    @GetMapping(value = "/{id}")
    public Logistica getById(@PathVariable("id") Long id) {
        return service.getById(id);
    }

    @PostMapping(value = "/bodega")
    public ResponseEntity<Object> saveBodega(@Valid @RequestBody LogisticaDTO dto) {
        Map<String, String> errors = new HashMap<>();

        if(dto.getPlacaVehiculo() == null){
            errors.put("placaVehiculo","Placa de vehículo no puede ser nulo");
        } else if(dto.getPlacaVehiculo().length() < 6 || dto.getPlacaVehiculo().length() > 6) {
            errors.put("placaVehiculo","Placa de vehículo debe de tener 6 caracteres");
        }else {
            Pattern pat = Pattern.compile("[a-zA-Z]{3}[0-9]{3}");
            Matcher mat = pat.matcher(dto.getPlacaVehiculo());
            if (!mat.matches()) {
                errors.put("placaVehiculo", "Placa de vehículo debe corresponder a 3 letras iniciales y 3 números finales");
            }
        }
        if(!errors.isEmpty()){
            return new ResponseEntity<Object>(errors, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Object>(service.saveBodega(dto), HttpStatus.OK);
    }

    @PostMapping(value = "/puerto")
    public ResponseEntity<Object> savePuerto(@Valid @RequestBody LogisticaDTO dto) {
        Map<String, String> errors = new HashMap<>();

        if(dto.getNumeroFlota() == null){
            errors.put("numeroFlota","Número de flota no puede ser nulo");
        } else if(dto.getNumeroFlota().length() > 8 || dto.getNumeroFlota().length() < 8) {
            errors.put("numeroFlota","Número de flota debe de tener 8 caracteres");
        }else {
            Pattern pat = Pattern.compile("[a-zA-Z]{3}[0-9]{4}[a-zA-Z]{1}");
            Matcher mat = pat.matcher(dto.getNumeroFlota());
            if (!mat.matches()) {
                errors.put("numeroFlota", "Número de flota debe corresponder a 3 letras iniciales y 4 números y finalizando con una letra)");
            }
        }
        if(!errors.isEmpty()){
            return new ResponseEntity<Object>(errors, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Object>(service.savePuerto(dto), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable Long id) {
        service.delete(id);
    }

}
