package sv.com.ingeneo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sv.com.ingeneo.dto.ClienteDTO;
import sv.com.ingeneo.model.Cliente;
import sv.com.ingeneo.service.ClienteService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    ClienteService service;

    @GetMapping(name = "/")
    public List<Cliente> getAll(){
        return service.getAll();
    }

    @GetMapping(value = "/{id}")
    public Cliente getById(@PathVariable("id") Long id) {
        return service.getById(id);
    }

    @PostMapping(value = "/")
    public Cliente save(@Valid @RequestBody ClienteDTO dto) {
        return service.save(service.convertToEntity(dto));
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable Long id) {
        service.delete(id);
    }

    @GetMapping(value = "/findbycodigo/{codigo}")
    public Cliente getById(@PathVariable("codigo") String codigo) {
        return service.getByCodigo(codigo);
    }

}
