package sv.com.ingeneo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sv.com.ingeneo.dto.DestinoDTO;
import sv.com.ingeneo.model.Destino;
import sv.com.ingeneo.model.DestinoBodega;
import sv.com.ingeneo.model.DestinoPuerto;
import sv.com.ingeneo.service.DestinoService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/destino")
public class DestinoController {

    final static Logger logger = LoggerFactory.getLogger(DestinoController.class);

    @Autowired
    DestinoService service;

    @GetMapping(name = "/")
    public List<Destino> getAll(){
        return service.getAll();
    }

    @GetMapping(value = "/{id}")
    public Destino getById(@PathVariable("id") Long id) {
        return service.getById(id);
    }

    @PostMapping(value = "/bodega")
    public Destino saveBodega(@Valid @RequestBody DestinoDTO dto) {
        logger.info(dto.toString());
        return service.save(service.convertToBodega(dto));
    }

    @PostMapping(value = "/puerto")
    public Destino savePuerto(@Valid @RequestBody DestinoDTO dto) {
        logger.info(dto.toString());
        return service.save(service.convertToPuerto(dto));
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable Long id) {
        service.delete(id);
    }

    @GetMapping(value = "/findbycodigo/{codigo}")
    public Destino getById(@PathVariable("codigo") String codigo) {
        return service.getByCodigo(codigo);
    }

    @GetMapping(value = "/allBodega")
    public List<DestinoBodega> getAllBodega(){
        return service.getAllBodega();
    }

    @GetMapping(value = "/allPuerto")
    public List<DestinoPuerto> getAllPuerto(){
        return service.getAllPuerto();
    }
}
