package sv.com.ingeneo.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import sv.com.ingeneo.constante.TipoDestino;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = TipoDestino.PUERTO)
public class LogisticaPuerto extends Logistica{

}
