package sv.com.ingeneo.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "destino", schema = "ingeneo")
@Inheritance( strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn( name="tipo")
public class Destino {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "codigo")
    private String codigo;

    @Column(name = "es_nacional")
    private Boolean esNacional;

    @Column(name = "tipo", updatable = false, insertable = false)
    private String tipo;

    public Destino() {
    }
}
