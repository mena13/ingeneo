package sv.com.ingeneo.model;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@Table(name = "logistica", schema = "ingeneo")
@Inheritance( strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn( name="tipo")
public class Logistica {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "cantidad_producto")
    private Integer cantidadProducto;

    @Column(name = "placa_flota")
    private String placaFlota;

    @Column(name = "precio_envio")
    private BigDecimal precioEnvio;

    @Column(name = "precio_normal")
    private BigDecimal precioNormal;

    @Column(name = "descuento")
    private BigDecimal descuento;

    @Column(name = "numero_guia")
    private String numeroGuia;

    @Column(name = "fecha_registro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRegistro;

    @Column(name = "fecha_entrega")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEntrega;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name ="id_destino_entrega")
    private Destino destino = new Destino();

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_tipo_producto")
    private TipoProducto tipoProducto;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_cliente")
    private Cliente cliente = new Cliente();

}
