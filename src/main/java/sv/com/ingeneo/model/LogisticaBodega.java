package sv.com.ingeneo.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import sv.com.ingeneo.constante.TipoDestino;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Optional;

@Entity
@DiscriminatorValue(value = TipoDestino.BODEGA)
public class LogisticaBodega extends Logistica{
    public LogisticaBodega() {
    }

}
