package sv.com.ingeneo.model;


import lombok.Data;
import lombok.NoArgsConstructor;
import sv.com.ingeneo.constante.TipoDestino;


import javax.persistence.Entity;
import javax.persistence.DiscriminatorValue;


@Entity
@DiscriminatorValue(value = TipoDestino.PUERTO)
public class DestinoPuerto extends Destino{

}
