package sv.com.ingeneo.service;

import sv.com.ingeneo.dto.DestinoDTO;
import sv.com.ingeneo.model.Destino;
import sv.com.ingeneo.model.DestinoBodega;
import sv.com.ingeneo.model.DestinoPuerto;

import java.util.List;

public interface DestinoService {

    List<Destino> getAll();

    List<DestinoBodega> getAllBodega();

    List<DestinoPuerto> getAllPuerto();

    Destino save(Destino destino);

    void delete(Long id);

    Destino getById(Long id);

    Destino getByCodigo(String codigo);

    List<Destino> getByDestino(Destino destino);

    DestinoDTO convertToDto(Destino entity);

    Destino convertToEntity(DestinoDTO dto);

    DestinoBodega convertToBodega(DestinoDTO dto);

    DestinoPuerto convertToPuerto(DestinoDTO dto);
}
