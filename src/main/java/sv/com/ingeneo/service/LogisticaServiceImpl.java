package sv.com.ingeneo.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.java.Log;
import org.hibernate.Criteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import sv.com.ingeneo.dto.LogisticaDTO;
import sv.com.ingeneo.model.*;
import sv.com.ingeneo.repository.*;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class LogisticaServiceImpl implements LogisticaService{

    final static Logger logger = LoggerFactory.getLogger(LogisticaServiceImpl.class);

    @Autowired
    private LogisticaRepo repo;

    @Autowired
    private LogisticaBodegaRepo repoBodega;

    @Autowired
    private LogisticaPuertoRepo repoPuerto;

    @Autowired
    private ClienteRepo repoCliente;

    @Autowired
    private DestinoBodegaRepo repoDestinoBodega;

    @Autowired
    private DestinoPuertoRepo repoDestinoPuerto;

    @Autowired
    private TipoProductoRepo repoTipoProducto;

    @Autowired
    private ObjectMapper mapper;

    @Override
    public List<Logistica> getAll() {
        return (List<Logistica>) repo.findAll();
    }

    @Override
    public Logistica save(Logistica logistica) {
        repo.save(logistica);
        return logistica;
    }

    @Override
    public void delete(Long id) {
        repo.deleteById(id);
    }

    @Override
    public Logistica getById(Long id) {
        return repo.findById(id).get();
    }


    @Override
    public List<Logistica> getByLogistica(Logistica logistica) {
        return null;
    }

    @Override
    public LogisticaDTO convertToDto(Logistica entity) {
        return mapper.convertValue(entity, LogisticaDTO.class);
    }

    @Override
    public Logistica convertToEntity(LogisticaDTO dto) {
        return mapper.convertValue(dto, Logistica.class);
    }

    @Override
    public LogisticaBodega saveBodega(LogisticaDTO dto) {
        LogisticaBodega entity = mapper.convertValue(dto,LogisticaBodega.class);
        entity.setCliente(repoCliente.findById(dto.getClienteId()).get());
        entity.setTipoProducto(repoTipoProducto.findById(dto.getTipoProductoId()).get());
        entity.setDestino(repoDestinoBodega.findById(dto.getDestinoId()).get());
        entity.setFechaRegistro(new Date());
        if(entity.getCantidadProducto()>=10){
            entity.setDescuento(entity.getPrecioEnvio().multiply(BigDecimal.valueOf(0.05)));
            entity.setPrecioNormal(entity.getPrecioEnvio());
            entity.setPrecioEnvio(entity.getPrecioNormal().subtract(entity.getDescuento()));
        }
        entity.setPlacaFlota(dto.getPlacaVehiculo());
        return repoBodega.save(entity);
    }

    @Override
    public LogisticaPuerto savePuerto(LogisticaDTO dto) {
        LogisticaPuerto entity = mapper.convertValue(dto,LogisticaPuerto.class);
        entity.setCliente(repoCliente.findById(dto.getClienteId()).get());
        entity.setTipoProducto(repoTipoProducto.findById(dto.getTipoProductoId()).get());
        entity.setDestino(repoDestinoPuerto.findById(dto.getDestinoId()).get());
        entity.setFechaRegistro(new Date());
        if(entity.getCantidadProducto()>=10){
            entity.setDescuento(entity.getPrecioEnvio().multiply(BigDecimal.valueOf(0.05)));
            entity.setPrecioNormal(entity.getPrecioEnvio());
            entity.setPrecioEnvio(entity.getPrecioNormal().subtract(entity.getDescuento()));
        }
        entity.setPlacaFlota(dto.getNumeroFlota());
        return repoPuerto.save(entity);
    }
}
