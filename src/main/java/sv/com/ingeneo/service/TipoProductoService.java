package sv.com.ingeneo.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import sv.com.ingeneo.dto.TipoProductoDTO;
import sv.com.ingeneo.model.TipoProducto;

import java.util.List;

public interface TipoProductoService {

    List<TipoProducto> getAll();

    TipoProducto save(TipoProducto tipoProducto);

    void delete(Long id);

    TipoProducto getById(Long id);

    TipoProducto getByCodigo(String codigo);

    List<TipoProducto> getByTipoProducto(TipoProducto tipoProducto);

    TipoProductoDTO convertToDto(TipoProducto entity);

    TipoProducto convertToEntity(TipoProductoDTO dto);
}
