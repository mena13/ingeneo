package sv.com.ingeneo.service;

import sv.com.ingeneo.dto.ClienteDTO;
import sv.com.ingeneo.model.Cliente;

import java.util.List;

public interface ClienteService {

    List<Cliente> getAll();

    Cliente save(Cliente cliente);

    void delete(Long id);

    Cliente getById(Long id);

    Cliente getByCodigo(String codigo);

    List<Cliente> getByCliente(Cliente cliente);

    ClienteDTO convertToDto(Cliente entity);

    Cliente convertToEntity(ClienteDTO dto);
}
