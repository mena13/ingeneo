package sv.com.ingeneo.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sv.com.ingeneo.dto.ClienteDTO;
import sv.com.ingeneo.model.Cliente;
import sv.com.ingeneo.repository.ClienteRepo;

import java.util.List;

@Service
public class ClienteServiceImpl implements ClienteService{

    @Autowired
    private ClienteRepo repo;

    @Autowired
    private ObjectMapper mapper;

    @Override
    public List<Cliente> getAll() {
        return (List<Cliente>) repo.findAll();
    }

    @Override
    public Cliente save(Cliente cliente) {
        repo.save(cliente);
        return cliente;
    }

    @Override
    public void delete(Long id) {
        repo.deleteById(id);
    }

    @Override
    public Cliente getById(Long id) {
        return repo.findById(id).get();
    }

    @Override
    public Cliente getByCodigo(String codigo) {
        return repo.findByCodigo(codigo);
    }

    @Override
    public List<Cliente> getByCliente(Cliente cliente) {
        return null;
    }

    @Override
    public ClienteDTO convertToDto(Cliente entity) {
        return mapper.convertValue(entity, ClienteDTO.class);
    }

    @Override
    public Cliente convertToEntity(ClienteDTO dto) {
        return mapper.convertValue(dto, Cliente.class);
    }
}
