package sv.com.ingeneo.service;

import sv.com.ingeneo.dto.LogisticaDTO;
import sv.com.ingeneo.model.Logistica;
import sv.com.ingeneo.model.LogisticaBodega;
import sv.com.ingeneo.model.LogisticaPuerto;

import java.util.List;

public interface LogisticaService {

    List<Logistica> getAll();

    Logistica save(Logistica logistica);

    void delete(Long id);

    Logistica getById(Long id);

    List<Logistica> getByLogistica(Logistica logistica);

    LogisticaDTO convertToDto(Logistica entity);

    Logistica convertToEntity(LogisticaDTO dto);

    LogisticaBodega saveBodega(LogisticaDTO entity);

    Logistica savePuerto(LogisticaDTO entity);

}
