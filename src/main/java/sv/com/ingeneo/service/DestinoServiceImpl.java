package sv.com.ingeneo.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sv.com.ingeneo.dto.DestinoDTO;
import sv.com.ingeneo.model.Destino;
import sv.com.ingeneo.model.DestinoBodega;
import sv.com.ingeneo.model.DestinoPuerto;
import sv.com.ingeneo.repository.DestinoBodegaRepo;
import sv.com.ingeneo.repository.DestinoPuertoRepo;
import sv.com.ingeneo.repository.DestinoRepo;

import java.util.List;

@Service
public class DestinoServiceImpl implements DestinoService{

    @Autowired
    private DestinoRepo repo;

    @Autowired
    private DestinoBodegaRepo repoBodega;

    @Autowired
    private DestinoPuertoRepo repoPuerto;

    @Autowired
    private ObjectMapper mapper;

    @Override
    public List<Destino> getAll() {
        return (List<Destino>) repo.findAll();
    }

    @Override
    public List<DestinoBodega> getAllBodega() {
        return  (List<DestinoBodega>) repoBodega.findAll();
    }

    @Override
    public List<DestinoPuerto> getAllPuerto() {
        return (List<DestinoPuerto>) repoPuerto.findAll();
    }

    @Override
    public Destino save(Destino destino) {
        repo.save(destino);
        return destino;
    }

    @Override
    public void delete(Long id) {
        repo.deleteById(id);
    }

    @Override
    public Destino getById(Long id) {
        return repo.findById(id).get();
    }

    @Override
    public Destino getByCodigo(String codigo) {
        return repo.findByCodigo(codigo);
    }

    @Override
    public List<Destino> getByDestino(Destino destino) {
        return null;
    }

    @Override
    public DestinoDTO convertToDto(Destino entity) {
        return mapper.convertValue(entity, DestinoDTO.class);
    }

    @Override
    public Destino convertToEntity(DestinoDTO dto) {
        return mapper.convertValue(dto, Destino.class);
    }

    @Override
    public DestinoBodega convertToBodega(DestinoDTO dto) {
        return mapper.convertValue(dto, DestinoBodega.class);
    }

    @Override
    public DestinoPuerto convertToPuerto(DestinoDTO dto) {
        return mapper.convertValue(dto, DestinoPuerto.class);
    }
}
