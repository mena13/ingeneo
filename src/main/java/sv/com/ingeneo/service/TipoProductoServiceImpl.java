package sv.com.ingeneo.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sv.com.ingeneo.dto.TipoProductoDTO;
import sv.com.ingeneo.model.TipoProducto;
import sv.com.ingeneo.repository.TipoProductoRepo;

import java.util.List;

@Service
public class TipoProductoServiceImpl implements TipoProductoService{

    @Autowired
    private TipoProductoRepo repo;

    @Autowired
    private ObjectMapper mapper;

    @Override
    public List<TipoProducto> getAll() {
        return (List<TipoProducto>) repo.findAll();
    }

    @Override
    public TipoProducto save(TipoProducto tipoProducto) {
        repo.save(tipoProducto);
        return tipoProducto;
    }

    @Override
    public void delete(Long id) {
        repo.deleteById(id);
    }

    @Override
    public TipoProducto getById(Long id) {
        return repo.findById(id).get();
    }

    @Override
    public TipoProducto getByCodigo(String codigo) {
        return repo.findByCodigo(codigo);
    }

    @Override
    public List<TipoProducto> getByTipoProducto(TipoProducto tipoProducto) {
        return null;
    }

    @Override
    public TipoProductoDTO convertToDto(TipoProducto entity) {
        return mapper.convertValue(entity, TipoProductoDTO.class);
    }

    @Override
    public TipoProducto convertToEntity(TipoProductoDTO dto) {
        return mapper.convertValue(dto, TipoProducto.class);
    }
}
