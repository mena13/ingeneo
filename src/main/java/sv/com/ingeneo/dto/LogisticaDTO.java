package sv.com.ingeneo.dto;

import lombok.Data;
import sv.com.ingeneo.model.Destino;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class LogisticaDTO {

    private Long id;

    @NotNull(message = "Cantidad de producto no puede ir nulo")
    private Integer cantidadProducto;

    private String placaVehiculo;

    private String numeroFlota;

    @NotNull(message = "Precio de envio no pueder ir nulo")
    private BigDecimal precioEnvio;

    @NotNull(message = "Número de guía  no pueder ir nulo")
    @NotBlank(message = "Número de guía  no puede ir vacio")
    @Size(min = 10, max = 10, message = "Número de guía debe ser de 10 caracteres")
    private String numeroGuia;

    private Date fechaRegistro;

    @NotNull(message = "Fecha de entrega de envio no pueder ir nulo")
    private String fechaEntrega;

    @NotNull(message = "Destino no pueder ir nulo")
    private Long destinoId;

    @NotNull(message = "Tipo de producto no pueder ir nulo")
    private Long tipoProductoId;

    @NotNull(message = "Cliente no pueder ir nulo")
    private Long clienteId;

    private BigDecimal precioNormal;

    private BigDecimal descuento;
}
