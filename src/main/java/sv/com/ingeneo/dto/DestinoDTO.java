package sv.com.ingeneo.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class DestinoDTO {

    private Long id;

    @NotBlank(message = "Nombre no puede ser vacio")
    @NotNull(message = "Nombre no puede ser null")
    private String nombre;

    @NotBlank(message = "Código no puede ser vacio")
    @NotNull(message = "Código no puede ser null")
    @Size(min = 1, max = 10, message = "Código no puede mayor a 10 caracteres")
    private String codigo;

    @NotNull(message = "Es Nacional no pueder ser null")
    private Boolean esNacional;

    private String tipo;
}
